package francois.rabanel.tp5.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import francois.rabanel.tp5.models.Teacher;

public class DBHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "cnam.db";
    private static final int DATABASE_VERSION = 1;
    public static final String TABLE_NAME = "professors";
    public static final String COLUMN_ID = "Id";
    public static final String COLUMN_LASTNAME = "Lastname";
    public static final String COLUMN_FIRSTNAME = "Firstname";
    public static final String COLUMN_EMAIL = "Email";
    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    private static final String CREATION_TEACHER_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
            COLUMN_ID + " integer primary key autoincrement, " +
            COLUMN_LASTNAME + " text not null, " +
            COLUMN_FIRSTNAME + " text not null, " +
            COLUMN_EMAIL + " text not null);";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATION_TEACHER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion == newVersion)
            return;
        // Simplest implementation is to drop all old tables and recreate them
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME + ";");
        onCreate(db);
    }

    public void addTeacher(Teacher teacher) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_FIRSTNAME, teacher.getFirstname());
        values.put(COLUMN_LASTNAME, teacher.getLastname());
        values.put(COLUMN_EMAIL, teacher.getEmail());

        // Inserting Row
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public ArrayList<Teacher> getTeachers(){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Teacher> teachers = new ArrayList<>();
        Cursor  cursor = db.rawQuery("select * from " + TABLE_NAME,null);

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String firstname = cursor.getString(cursor.getColumnIndex(COLUMN_FIRSTNAME));
                String lastname = cursor.getString(cursor.getColumnIndex(COLUMN_LASTNAME));
                String email = cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL));

                Teacher obj = new Teacher(firstname, lastname, email);
                teachers.add(obj);
                cursor.moveToNext();
            }
        }
        return  teachers;
    }

    public int getTeacherFromEmail(String email) {
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = "select id from " + TABLE_NAME + " where " + COLUMN_EMAIL + "='" + email + "'";
        Log.d("-----------------------------", "where: " + whereClause);
        Cursor cursor = db.rawQuery(whereClause ,null);
        Log.d("-----------------------------", "cursor: " + cursor);

        // Fonctionne pas. Récupère -1
        if(cursor.getCount() > 0) {
            return cursor.getInt(0);
        } else {
            return 0;
        }
    }
    public void deleteTeacher(String email){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, COLUMN_EMAIL + " = ?", new String[]{email});
        db.close();
    }
}