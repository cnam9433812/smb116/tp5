package francois.rabanel.tp5.models;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class Teacher implements Parcelable {
    private String lastname;
    private String firstname;
    private String email;

    public Teacher(String lastname, String firstname, String email) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.email = email;
    }

    public String getLastname(){ return lastname; }
    public String getFirstname(){ return firstname; }
    public String getEmail(){ return email; }
    public String getString(){
        return firstname + " " + lastname + " " + email;
    }
    public void setLastname(String lastname) {this.lastname = lastname;}
    public void setFirstname(String firstname) {this.firstname = firstname;}
    public void setEmail(String email) {this.email = email;}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeString(this.lastname);
        parcel.writeString(this.firstname);
        parcel.writeString(this.email);
    }
    protected Teacher (Parcel in) {
        this.firstname = in.readString();
        this.lastname = in.readString();
        this.email = in.readString();
    }

    public static final Creator<Teacher> CREATOR = new Creator<Teacher>() {
        @Override
        public Teacher createFromParcel(Parcel in) {
            return new Teacher(in);
        }
        @Override
        public Teacher[] newArray(int size) {
            return new Teacher[size];
        }
    };

    public static Teacher fromContentValues(ContentValues values){
        final Teacher teacher;

        String firstname = values.getAsString("firstname");
        String lastname = values.getAsString("lastname");
        String email = values.getAsString("email");

        teacher = new Teacher(firstname, lastname, email);
        return teacher;
    }
}