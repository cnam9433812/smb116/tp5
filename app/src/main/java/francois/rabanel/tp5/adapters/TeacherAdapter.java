package francois.rabanel.tp5.adapters;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import francois.rabanel.tp5.R;
import francois.rabanel.tp5.models.Teacher;

public class TeacherAdapter extends ArrayAdapter {
    private final Context _context;
    private ArrayList<Teacher> _teachers;

    public TeacherAdapter(Context context, int resource, ArrayList<Teacher> teachers) {
        super(context, resource, teachers);
        this._context = context;
        this._teachers = teachers;
    }

    public Teacher getTeacher(int position){
        return _teachers.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.rowteacher, parent, false);
        } else {
            convertView = (LinearLayout) convertView;
        }

        TextView viewTitle = (TextView) convertView.findViewById(R.id.firstname);
        viewTitle.setText(_teachers.get(position).getFirstname());

        return convertView;
    }
}