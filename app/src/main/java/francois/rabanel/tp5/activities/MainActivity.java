package francois.rabanel.tp5.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import francois.rabanel.tp5.R;
import francois.rabanel.tp5.adapters.TeacherAdapter;
import francois.rabanel.tp5.database.DBHelper;
import francois.rabanel.tp5.models.Teacher;
import francois.rabanel.tp5.activities.AddActivity;

public class MainActivity extends AppCompatActivity {
    private ListView list;
    private Button addButton;
    private ArrayList<Teacher> teachers;
    private TeacherAdapter adapter;
    private DBHelper database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        database = new DBHelper(this);
        Log.d("-------------------------", "CA REVIENT ICI ON CREATE");

        list = (ListView) findViewById(R.id.listView1);
        addButton = (Button) findViewById(R.id.add_button);

        // On change d'activité si on veut ajouter un professeur.
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addIntent = new Intent(getApplicationContext(), AddActivity.class);
                startActivity(addIntent);
            }
        });

        // Si on clique sur un item de la listView, on affiche un toast avec les infos.
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Teacher teacher = adapter.getTeacher(i);
                Toast.makeText(
                        getApplicationContext(),
                        teacher.getString(),
                        Toast.LENGTH_SHORT
                ).show();

                Intent intent = new Intent(getApplicationContext(), DetailsActivity.class);
                intent.putExtra("obj", teacher);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        // On récupère tout le contenu de la table professors quand on revient dans l'activité
        // Et on met à jour notre listView
        teachers = database.getTeachers();
        adapter = new TeacherAdapter(getApplicationContext(), R.layout.activity_main, teachers);
        adapter.notifyDataSetChanged();
        list.setAdapter(adapter);
        Log.d("-------------------------", "CA REVINET ICI ON RESUME");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("-------------------------", "CA REVINET ICI ON PAUSE");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("-------------------------", "CA REVINET ICI ON STOP");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("-------------------------", "CA REVINET ICI ON DESTROY");
    }
}