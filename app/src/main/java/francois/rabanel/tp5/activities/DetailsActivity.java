package francois.rabanel.tp5.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import francois.rabanel.tp5.R;
import francois.rabanel.tp5.database.DBHelper;
import francois.rabanel.tp5.models.Teacher;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Intent intent = getIntent();
        Teacher teacher = intent.getParcelableExtra("obj");

        TextView firstnameTextView = (TextView) findViewById(R.id.firstname_textview);
        firstnameTextView.setText(teacher.getFirstname());

        TextView lastnameTextView = (TextView) findViewById(R.id.lastname_textview);
        lastnameTextView.setText(teacher.getLastname());

        TextView emailTextView = (TextView) findViewById(R.id.email_textview);
        emailTextView.setText(teacher.getEmail());

        Button backButton = (Button) findViewById(R.id.back_button);
        Button deleteButton = (Button) findViewById(R.id.delete_button);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DBHelper database = new DBHelper(getApplicationContext());

                // on supprime de la base de donnée avant de revenir
                // dans l'activité principale.
                database.deleteTeacher(teacher.getEmail());
                Toast.makeText(
                        getApplicationContext(),
                        "Deleted.",
                        Toast.LENGTH_SHORT
                ).show();
                finish();
            }
        });
    }
}