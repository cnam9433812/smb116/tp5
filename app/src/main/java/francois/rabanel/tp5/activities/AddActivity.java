package francois.rabanel.tp5.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

import francois.rabanel.tp5.R;
import francois.rabanel.tp5.database.DBHelper;
import francois.rabanel.tp5.models.Teacher;

public class AddActivity extends AppCompatActivity {
    private Button validateButton;
    private EditText firstnameEditText, lastnameEditText, emailEditText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        validateButton = (Button) findViewById(R.id.validate_button);
        firstnameEditText = (EditText) findViewById(R.id.editTextFirstname);
        lastnameEditText = (EditText) findViewById(R.id.editTextLastname);
        emailEditText = (EditText) findViewById(R.id.editTextEmail);

        validateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Lorsqu'on clique sur valider, on récupère les infos entrées par l'utilisateur
                String firstnameValue = firstnameEditText.getText().toString();
                String lastnameValue = lastnameEditText.getText().toString();
                String emailValue = emailEditText.getText().toString();

                // On crée notre objet Teacher et on l'ajoute en base.
                Teacher teacher = new Teacher(lastnameValue, firstnameValue, emailValue);
                DBHelper database = new DBHelper(AddActivity.this);
                database.addTeacher(teacher);

                finish();
            }
        });
    }



}